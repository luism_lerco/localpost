import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { DataTablesModule } from 'angular-datatables';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RastreoComponent } from './components/rastreo/rastreo.component';
import { RegisterComponent } from './components/user/register/register.component';
import { LoginComponent } from './components/user/login/login.component';
import { InicioComponent } from './components/user/inicio/inicio.component';
import { MisEnviosComponent } from './components/user/mis-envios/mis-envios.component';
import { NuevoEnvioComponent } from './components/user/nuevo-envio/nuevo-envio.component';
import { MisDatosComponent } from './components/user/mis-datos/mis-datos.component';
import { FaqComponent } from './components/faq/faq.component';
import { AvisoDePrivacidadComponent } from './components/aviso-de-privacidad/aviso-de-privacidad.component';
import { TerminosYCondicionesComponent } from './components/terminos-y-condiciones/terminos-y-condiciones.component';
import { Page404Component } from './components/page404/page404.component';
import { FooterComponent } from './components/template-parts/footer/footer.component';
import { NavbaradminComponent } from './components/template-parts/navbaradmin/navbaradmin.component';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';


// Lib Paypal
import { NgxPayPalModule } from 'ngx-paypal';

// Lib PDF
// Import pdfmake-wrapper and the fonts to use
import { PdfMakeWrapper } from 'pdfmake-wrapper';
import * as pdfFonts from "pdfmake/build/vfs_fonts"; // fonts provided for pdfmake

// Set the fonts to use
PdfMakeWrapper.setFonts(pdfFonts);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    RastreoComponent,
    RegisterComponent,
    LoginComponent,
    InicioComponent,
    MisEnviosComponent,
    NuevoEnvioComponent,
    MisDatosComponent,
    FaqComponent,
    AvisoDePrivacidadComponent,
    TerminosYCondicionesComponent,
    Page404Component,
    FooterComponent,
    NavbaradminComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule, 
    FormsModule,
    HttpClientModule,
    NgxSpinnerModule, //modulo loading spinners
    NgxPayPalModule, //modulo paypal
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    DataTablesModule, //Data Tables
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
