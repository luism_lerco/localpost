import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from 'src/app/servicios/api/api.service';
import { ResponseInterface} from 'src/app/modelos/response.interface';
import { rastreoInterface} from 'src/app/modelos/rastreo.interface';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-rastreo',
  templateUrl: './rastreo.component.html',
  styleUrls: ['./rastreo.component.css']
})

export class RastreoComponent implements OnInit {

  name = 'Set iframe source';
  url: string = "https://guia.localpost.com.mx/index.html";
  urlSafe: SafeResourceUrl;

  item!:rastreoInterface[];
  item2!:rastreoInterface[];

  rastreoForm = new FormGroup({
    guia : new FormControl('')
  })

  constructor(private api:ApiService, public sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.urlSafe= this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
  } 


  rastreo(guia:any){
    //console.log(guia);
    var numberGuia = Number(guia);
    //console.log(numberGuia);
    this.api.rastreoPaquete(36).subscribe(data =>{
    //this.api.rastreoPaquete(numberGuia).subscribe(data =>{
      let dataResponse:ResponseInterface = data;
      console.log(data);
      for(let item of dataResponse.data){
        for(let item2 of item){
          console.log(item2);
        }
      }
      //this.item = dataResponse.data;
      /*for(this.item of dataResponse.data ){
        //Primer Array
        for(let item2 of this.item){
          //Segunda array
          let item3 = item2;
          console.log(item3); 
        }
      }*/
      
    })

  }

}
