import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../servicios/api/api.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UsuarioInterface } from 'src/app/modelos/usuario.interface';
import { ResponseInterface } from 'src/app/modelos/response.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-mis-datos',
  templateUrl: './mis-datos.component.html',
  styleUrls: ['./mis-datos.component.css']
})
export class MisDatosComponent implements OnInit {
  
  
  

  constructor(private api:ApiService, private alertas:AlertasService, private router:Router) { }

  datosUsuario!:UsuarioInterface;
  editarForm = new FormGroup({
    token: new FormControl(''),
    id : new FormControl(''),
    nombre : new FormControl(''),
    apellidos : new FormControl(''),
    email : new FormControl(''),
    empresa : new FormControl(''),
    giro_empresa : new FormControl(''),
    cargo : new FormControl(''),
    //rfc : new FormControl(''),
    telefono : new FormControl(''),
  })
  

  ngOnInit(): void {
    this.checkLocalStorage();
    let toke = localStorage.getItem('token');
    let usr_id = localStorage.getItem('user_id');
    var usr_id_num = Number(usr_id);
    this.api.getUsuario(usr_id_num).subscribe(data =>{
    //this.api.getUsuario(6).subscribe(data =>{
      let dataResponse:ResponseInterface = data;
      this.datosUsuario = dataResponse.data;
      //console.log(dataResponse.data);
      this.editarForm.setValue({
        'token': toke,
        'id': usr_id,
        'nombre': this.datosUsuario.nombre,
        'apellidos': this.datosUsuario.apellidos,
        'email': this.datosUsuario.email,
        'empresa': this.datosUsuario.empresa,
        'giro_empresa': this.datosUsuario.giro_empresa,
        'cargo': this.datosUsuario.cargo,
        //'rfc': this.datosUsuario.rfc,
        'telefono': this.datosUsuario.telefono
      });
      //console.log(this.editarForm.value);
    })
    
  }

  

  checkLocalStorage(){
    if (localStorage.getItem('token')) {
      this.router.navigate(['user/mis-datos']);
    }
    else{
      this.router.navigate(['user/login']);
    }
  }

  onUpdate(form:UsuarioInterface){
    console.log(form);
    this.api.updateCliente(form).subscribe( data =>{
      console.log(data);
      let dataResponse:ResponseInterface = data;
      if (dataResponse.code == "202") {
        this.alertas.showSuccess('Datos actualizados','Hecho');
      }
    })
  }
  

}
