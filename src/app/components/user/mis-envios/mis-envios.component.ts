import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../servicios/api/api.service';
import { ListaEnviosInterface} from '../../../modelos/listaenvios.interface';
import { ResponseInterface } from '../../../modelos/response.interface';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
//import 'rxjs/add/operator/map';

//Librerias para crear pdf
import { PdfMakeWrapper, Txt, QR, Stack, Table, Img } from 'pdfmake-wrapper';

@Component({
  selector: 'app-mis-envios',
  templateUrl: './mis-envios.component.html',
  styleUrls: ['./mis-envios.component.css'],
})

export class MisEnviosComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  envios!:ListaEnviosInterface[];

  dtTrigger: Subject<any> = new Subject<any>();
  

  constructor(private api:ApiService, private router:Router) { }

  ngOnInit(): void {
    this.checkLocalStorage();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      searching: false,
      language: {
        emptyTable: '',
        zeroRecords: 'No hay coincidencias',
        lengthMenu: 'Mostrar _MENU_ elementos',
        search: 'Buscar:',
        info: 'De _START_ a _END_ de _TOTAL_ elementos',
        infoEmpty: 'De 0 a 0 de 0 elementos',
        infoFiltered: '(filtrados de _MAX_ elementos totales)',
        paginate: {
          first: '<< ',
          last: ' >>',
          next: ' >',
          previous: '< '
        }
      }
    };
    let usr_id = localStorage.getItem('user_id');
    var usr_id_num = Number(usr_id);
    //console.log(usr_id_num);
    this.api.getAllEnvios(usr_id_num).subscribe(data =>{
    //this.api.getAllEnvios(6).subscribe(data =>{
      
      let dataResponse:ResponseInterface = data;
      console.log(dataResponse);
      this.envios = dataResponse.data;
      //esta linea es para la paginación
      this.dtTrigger.next();
    })

  }

  //Verificamos si el usuario esta logueado
  checkLocalStorage(){
    if (localStorage.getItem('token')) {
      this.router.navigate(['user/mis-envios']);
    }
    else{
      this.router.navigate(['user/login']);
    }
  }

  //Verificamos si podemos mostrar el boton de la guia
  mostrarBoton(status_p:string){
    if (status_p == 'Recolectado / Recaudado') {
      return true;
    }
    else{
      return false;
    }
  }

  //Creamos el PDF
  generatePDF(folio:string, tracked:string, remitente:string, direccion_origen:string, destinatario:string, direccion_destino:string,telefono_remitente:string, peso:string){

    let nom_arch:string = "Guia_Local_Post-" + folio;
    
    const pdf = new PdfMakeWrapper();
    //configuraciones del PDF
    pdf.pageSize('A5');
    pdf.pageOrientation('landscape');
    pdf.pageMargins([ 40, 60, 40, 60 ]);
    //Comenzamos la declaración de la imagen para usarla en la guia
    new Img('/assets/img/logo_guia.png').build().then( img => {

    //Creamos el contenido para el PDF
    pdf.add(
      //new QR(tracked).end
      new Table([
        [
          new QR(tracked).end,
          new Stack([
            //Aqui agregamos la imagen del logo
            img,
            new Txt('N° de guía').fontSize(20).bold().end, 
            new Txt(folio).fontSize(15).bold().end
          ]).alignment('center').end,
          new QR(tracked).end 
        ]
      ]).layout('noBorders').widths([80, 290, 80]).end
    );
    pdf.add(
      pdf.ln(2)
    );
    pdf.add(
      //Datos de la guía
      new Table([
        [ 
          new Txt('Remitente: ').bold().end,
          //Datos variables 
          remitente
        ],
        [ 
          new Txt('Dirección Remitente: ').bold().end, 
          direccion_origen
        ],
        [ 
          new Txt('Destinatario: ').bold().end, 
          destinatario
        ],
        [ 
          new Txt('Dirección Destinatario: ').bold().end, 
          direccion_destino
        ],
        [ 
          new Txt('Teléfono: ').bold().end, 
          telefono_remitente
        ],
        [ 
          new Txt('Peso: ').bold().end, 
          peso
        ]
      ]).layout('noBorders').end
    );
    // copia de la guia comienza desde aqui
    pdf.add(
      new Table([
        [
          new QR(tracked).end,
          new Stack([
            img,
            new Txt('N° de guía').fontSize(20).bold().end, 
            new Txt(folio).fontSize(15).bold().end
          ]).alignment('center').end,
          new QR(tracked).end 
        ]
      ]).layout('noBorders').widths([80, 280, 80]).end
    );
    pdf.add(
      pdf.ln(2)
    );
    pdf.add(
      //Datos de la guía
      new Table([
        [ 
          new Txt('Remitente: ').bold().end,
          //Datos variables 
          remitente
        ],
        [ 
          new Txt('Dirección Remitente: ').bold().end, 
          direccion_origen
        ],
        [ 
          new Txt('Destinatario: ').bold().end, 
          destinatario
        ],
        [ 
          new Txt('Dirección Destinatario: ').bold().end, 
          direccion_destino
        ],
        [ 
          new Txt('Teléfono: ').bold().end, 
          telefono_remitente
        ],
        [ 
          new Txt('Peso: ').bold().end, 
          peso
        ]
      ]).layout('noBorders').end
    );

    //Configuramos salida del archivo
    pdf.create().download(nom_arch);
    //pdf.create().open();
    });
  }
  

}
