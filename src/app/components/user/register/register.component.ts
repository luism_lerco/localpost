import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService} from 'src/app/servicios/api/api.service';
import { registerInterface} from 'src/app/modelos/register.interface';
import { ResponseInterface} from 'src/app/modelos/response.interface';

import { AlertasService } from 'src/app/servicios/alertas/alertas.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm = new FormGroup({
    nombre : new FormControl('', Validators.required),
    apellidos : new FormControl('', Validators.required),
    email : new FormControl('', Validators.required),
    empresa : new FormControl('', Validators.required),
    giro_empresa : new FormControl('', Validators.required),
    cargo : new FormControl('', Validators.required),
    rfc : new FormControl(''),
    telefono : new FormControl('', Validators.required),
    password : new FormControl('', Validators.required),
  })

  constructor( private api:ApiService, private router:Router,private alertas:AlertasService ) { }

  //errorStatus:boolean = false;
  //errorMsj: any = "";
  //errorMsj2: any = "";
  //errorMsj3: any = "";

  ngOnInit(): void {
  }

  onRegister(form:registerInterface){
    console.log(form);
    this.api.registerUser(form).subscribe( data =>{
      console.log(data);
      let dataResponse:ResponseInterface = data;
      if (dataResponse.code == "202") {
        localStorage.setItem("token", dataResponse.data.token);
        // Guardamos el ID del usuario para las demas operaciones
        localStorage.setItem("user_id", dataResponse.data.id);
        //let usr_id = localStorage.getItem('user_id');
        this.alertas.showSuccess('Registrado correctamente' , 'Hecho');
        this.router.navigate(['user/nuevo-envio']);
      }
      if (dataResponse.code == "10") {
        this.alertas.showError(dataResponse.data.telefono_movil, dataResponse.message);
        this.alertas.showError(dataResponse.data.email, dataResponse.message);
        //this.errorStatus = true;
        //this.errorMsj = dataResponse.message;
        //this.errorMsj2 = dataResponse.data.telefono_movil;
        //this.errorMsj3 = dataResponse.data.email;
      }
    })

  }

}
