import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../servicios/api/api.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ResponseInterface} from 'src/app/modelos/response.interface';
import { nuevoEnvioInterface} from 'src/app/modelos/nuevoenvio.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment'
//Paypal
import {IPayPalConfig, ICreateOrderRequest} from 'ngx-paypal';


@Component({
  selector: 'app-nuevo-envio',
  templateUrl: './nuevo-envio.component.html',
  styleUrls: ['./nuevo-envio.component.css']
})
export class NuevoEnvioComponent implements OnInit {

  public payPalConfig ? : IPayPalConfig;

  nuevoEnvioForm = new FormGroup({
    token: new FormControl(''),
    tipo_envio : new FormControl(''),
    cliente_emisor_id : new FormControl(''),
    receptor : new FormControl(''),
    direccion_origen : new FormControl(''),
    estado_origen : new FormControl(''),
    ciudad_origen : new FormControl(''),
    cp_origen : new FormControl(''),
    embalaje : new FormControl(''),
    peso : new FormControl(''),
    direccion_destino : new FormControl(''),
    estado_destino : new FormControl(''),
    ciudad_destino : new FormControl(''),
    cp_destino : new FormControl(''),
    fecha_recoleccion : new FormControl(''),
    fecha_entrega : new FormControl(''),
    //is_paid : new FormControl(''),
    factura : new FormControl(''),
    comentarios : new FormControl(''),
    total : new FormControl(''),
    subtotal : new FormControl(''),
    impuesto : new FormControl(''),
    telefono_receptor: new FormControl(''),
    pago: new FormControl(''),
    tipo_pago: new FormControl('')
  })

  tipo_emb = '';
  tipo_peso = '';
  //costo_final = new FormControl('');
  costo_final = '';
  costo_final_SINIVA = '';
  costo_final_IVA = 0;
  tipoEnvio = 10;
  tipoEnvio1 = 20;
  tipoEnvio2 = 30;
 
  isPaid = '';

  con_factura = '';

  modelChangeFn(e:any) {
    console.log(e);
    if (e == 'Menos de 1kg') {
      this.tipo_emb = 'Miniatura';
      //Lo repetimos para que muestre el boton de paypal si antes se indica que el paquete pesa mas de 20k
      this.pesoStatus = false;
    }
    if (e == '1 a 2kg') {
      this.tipo_emb = 'Size A';
      this.pesoStatus = false;
    }
    if (e == '3 a 5kg') {
      this.tipo_emb = 'Size B';
      this.pesoStatus = false;
    }
    if (e == '6 a 8kg') {
      this.tipo_emb = 'Size C';
      this.pesoStatus = false;
    }
    if (e == '9 a 15kg') {
      this.tipo_emb = 'Size D';
      this.pesoStatus = false;
    }
    if (e == '16 a 20kg') {
      this.tipo_emb = 'Size E';
      this.pesoStatus = false;
    }
    //Nuevo estado
    if (e == 'Más de 20kg') {
      this.tipo_emb = 'Entrega especial';
      //Mostramos el text area y deshabilitamos el boton de paypal
      this.pesoStatus = true;
    }
  }

  peso:string[]=["Menos de 1kg","1 a 2kg","3 a 5kg","6 a 8kg","9 a 15kg", "16 a 20kg", "Más de 20kg"];
  
  constructor(private api:ApiService, private alertas:AlertasService, private router:Router) { }

  //condicional para mostrar o no el boton de paypal
  pesoStatus: boolean = false;

  opcion1:any = '';
  opcion2:any = '';
  resultado:any = '';

  ngOnInit(): void {
    this.checkLocalStorage();
    this.initConfig();
    let toke = localStorage.getItem('token');
    let usr_id = localStorage.getItem('user_id');
    this.nuevoEnvioForm.patchValue({
      'token': toke,
      'cliente_emisor_id': usr_id
    })
    
  }

  checkLocalStorage(){
    if (localStorage.getItem('token')) {
      this.router.navigate(['user/nuevo-envio']);
    }
    else{
      this.router.navigate(['user/login']);
    }
  }

  calcularCosto(tipo_peso:any, tipo_emb:any):any{
    //this.tipoEnvio = 10;
    if (tipo_emb == 'Miniatura' && tipo_peso == 'Menos de 1kg') {
      if (this.con_factura == '1') {
        this.costo_final_SINIVA = '137';
        //agregarmos a variable para convertir a numero
        var stringToConvert = '137';
        //convertimos a numero
        var numberIVA = Number(stringToConvert);
        //calculamos iva
        numberIVA = numberIVA * 0.16
        this.costo_final_IVA = numberIVA;
        //lo sumamos al costo
        var numberCostoIVA = 137 + numberIVA;
        var numberRedIVA = numberCostoIVA.toFixed(2);
        var costoString = numberRedIVA.toString()

        return this.costo_final = costoString;
      }
      else{
        this.costo_final_SINIVA = '137';
        return this.costo_final = '137';
      }
      
    }
    if (tipo_emb == 'Size A' && tipo_peso == '1 a 2kg') {
      if (this.con_factura == '1'){
        this.costo_final_SINIVA = '147';
        //agregarmos a variable para convertir a numero
        var stringToConvert = '147';
        //convertimos a numero
        var numberIVA = Number(stringToConvert);
        //calculamos iva
        numberIVA = numberIVA * 0.16
        this.costo_final_IVA = numberIVA;
        //lo sumamos al costo
        var numberCostoIVA = 147 + numberIVA;
        var numberRedIVA = numberCostoIVA.toFixed(2);
        var costoString = numberRedIVA.toString()
        return this.costo_final = costoString;
      }
      else{
        this.costo_final_SINIVA = '147';
        return this.costo_final = '147';
      }
      
    }
    if (tipo_emb == 'Size B' && tipo_peso == '3 a 5kg') {
      if (this.con_factura == '1'){
        this.costo_final_SINIVA = '157';
        //agregarmos a variable para convertir a numero
        var stringToConvert = '157';
        //convertimos a numero
        var numberIVA = Number(stringToConvert);
        //calculamos iva
        numberIVA = numberIVA * 0.16
        this.costo_final_IVA = numberIVA;
        //lo sumamos al costo
        var numberCostoIVA = 157 + numberIVA;
        var numberRedIVA = numberCostoIVA.toFixed(2);
        var costoString = numberRedIVA.toString()
        return this.costo_final = costoString;
      }
      else{
        this.costo_final_SINIVA = '157';
        return this.costo_final = '157';
      }
    }
    if (tipo_emb == 'Size C' && tipo_peso == '6 a 8kg') {
      if (this.con_factura == '1'){
        this.costo_final_SINIVA = '167';
        //agregarmos a variable para convertir a numero
        var stringToConvert = '167';
        //convertimos a numero
        var numberIVA = Number(stringToConvert);
        //calculamos iva
        numberIVA = numberIVA * 0.16
        this.costo_final_IVA = numberIVA;
        //lo sumamos al costo
        var numberCostoIVA = 167 + numberIVA;
        var numberRedIVA = numberCostoIVA.toFixed(2);
        var costoString = numberRedIVA.toString()
        return this.costo_final = costoString;
      }
      else{
        this.costo_final_SINIVA = '167';
        return this.costo_final = '167';
      }
    }
    if (tipo_emb == 'Size D' && tipo_peso == '9 a 15kg') {
      if (this.con_factura == '1'){
        this.costo_final_SINIVA = '177';
        //agregarmos a variable para convertir a numero
        var stringToConvert = '177';
        //convertimos a numero
        var numberIVA = Number(stringToConvert);
        //calculamos iva
        numberIVA = numberIVA * 0.16
        this.costo_final_IVA = numberIVA;
        //lo sumamos al costo
        var numberCostoIVA = 177 + numberIVA;
        var numberRedIVA = numberCostoIVA.toFixed(2);
        var costoString = numberRedIVA.toString()
        return this.costo_final = costoString;
      }
      else{
        this.costo_final_SINIVA = '177';
        return this.costo_final = '177';
      }
    }
    if (tipo_emb == 'Size E' && tipo_peso == '16 a 20kg') {
      if (this.con_factura == '1'){
        this.costo_final_SINIVA = '187';
        //agregarmos a variable para convertir a numero
        var stringToConvert = '187';
        //convertimos a numero
        var numberIVA = Number(stringToConvert);
        //calculamos iva
        numberIVA = numberIVA * 0.16
        this.costo_final_IVA = numberIVA;
        //lo sumamos al costo
        var numberCostoIVA = 187 + numberIVA;
        var numberRedIVA = numberCostoIVA.toFixed(2);
        var costoString = numberRedIVA.toString()
        return this.costo_final = costoString;
      }
      else{
        this.costo_final_SINIVA = '187';
        return this.costo_final = '187';
      }
    }
  }

  //Funcion para calcular el IVA
  /*sacarIVA(costosinIva:string){
    var stringToConvert = costosinIva;
    //convertimos a numero
    var numberIVA = Number(stringToConvert);
    //calculamos iva
    numberIVA = numberIVA * 1.16
    //redondeamos el costo
    var numberRedIVA = numberIVA.toFixed(2);
    //regresamos el costo
    return numberRedIVA;
  }*/

  //Funcion para agregar impuesto
  agregraImpuesto(e:any){
    console.log(e.target.value);
    if (e.target.value == 1) {
      this.con_factura = '1';
    }
    if (e.target.value == 0) {
      this.con_factura = '0';
      this.costo_final_IVA = 0;
    }
  }

  registerEnvio(form:nuevoEnvioInterface){
    console.log(form)
    this.api.registerEnvio(form).subscribe( data =>{
      //console.log(data);
      let dataResponse:ResponseInterface = data;
      if (dataResponse.code == "202") {
        this.alertas.showSuccess('¡Gracias por tu confianza!','Hecho');
        this.router.navigate(['user/inicio']); 
      }
      if (dataResponse.code == "10") {
        this.alertas.showError(dataResponse.message, 'Error');
        this.alertas.showError(dataResponse.message.tipo_envio, 'Error');
      }
    })
  }

  //Funcion Paypal
  private initConfig(): void {
    this.payPalConfig = {
        currency: 'MXN',
        //Token nuevo
        clientId: environment.clientId,
        createOrderOnClient: (data) => < ICreateOrderRequest > {
            intent: 'CAPTURE',
            purchase_units: [{
                amount: {
                    currency_code: 'MXN',
                    //value: '250.00',
                    value: this.costo_final,
                    breakdown: {
                        item_total: {
                            currency_code: 'MXN',
                            //value: '250.00'
                            value: this.costo_final
                        }
                    }
                },
                items: [{
                    name: 'Envio CDMX Local Post',
                    quantity: '1',
                    category: 'DIGITAL_GOODS',
                    unit_amount: {
                        currency_code: 'MXN',
                        //value: '250.00',
                        value: this.costo_final,
                    },
                }]
            }]
        },
        advanced: {
            commit: 'true'
        },
        style: {
            label: 'paypal',
            layout: 'vertical'
        },
        onApprove: (data, actions) => {
            console.log('onApprove - transaction was approved, but not authorized', data, actions);
            actions.order.get().then((details:any) => {
                console.log('onApprove - you can get full order details inside onApprove: ', details);
            });

        },
        onClientAuthorization: (data) => {
            console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
            //Preparamos los datos adicionales para enviar en el formulario
            //agregamos el costo final y el tipo de pago
            this.nuevoEnvioForm.patchValue({
              'pago': this.costo_final,
              'tipo_pago': 70
            })
            //Una vez confirmado el pago mandamos los datos
            this.registerEnvio(this.nuevoEnvioForm.value);
        },
        onCancel: (data, actions) => {
            console.log('OnCancel', data, actions);

        },
        onError: err => {
            console.log('OnError', err);
        },
        onClick: (data, actions) => {
            console.log('onClick', data, actions);
        },
    };
}

}
