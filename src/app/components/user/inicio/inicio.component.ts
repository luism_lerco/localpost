import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../servicios/api/api.service';
import { ListaEnviosInterface} from '../../../modelos/listaenvios.interface';
import { ResponseInterface } from '../../../modelos/response.interface';
import { Router } from '@angular/router';

//Librerias para crear pdf
import { PdfMakeWrapper, Txt, QR, Stack, Table, Img } from 'pdfmake-wrapper';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})

export class InicioComponent implements OnInit {

  envios!:ListaEnviosInterface[];

  constructor (private api:ApiService, private router:Router) { }

  
  totales_envios = 0;
  totalcosto_envio = 0;
  costo_promedio:any = 0;
  costoPromedio:number = 0;

  ngOnInit(): void {
    this.checkLocalStorage();
    let usr_id = localStorage.getItem('user_id');
    var usr_id_num = Number(usr_id);
    
    this.api.getAllEnvios(usr_id_num).subscribe(data =>{
      
      let dataResponse:ResponseInterface = data;
      console.log(dataResponse.data);
      //this.envios = dataResponse.data.slice(0, 3);
      //Con esta línea obtenemos los últimos tres envios hechos
      this.envios = dataResponse.data.slice(Math.max (dataResponse.data.length - 3, 0));
      
      //obtenemos el número total de envios
      this.totales_envios = dataResponse.data.length;
      for(let item of dataResponse.data){
        var numberMonto = Number(item.monto);
        //sumamos el costo de todos los envios
        this.totalcosto_envio = this.totalcosto_envio + numberMonto;
      }

      //Sacamos el promedio total de los envios con respecto al numero de envios  
      this.costo_promedio = this.totalcosto_envio / this.totales_envios;
      var costoPromedio = Number(this.costo_promedio);
      var costoRedPromedio = costoPromedio.toFixed(2);
      this.costo_promedio =costoRedPromedio; 

    })
  }

  checkLocalStorage(){
    if (localStorage.getItem('token')) {
      this.router.navigate(['user/inicio']);
    }
    else{
      this.router.navigate(['user/login']);
    }
  }
  
  //Creamos el PDF
  generatePDF(folio:string, tracked:string, remitente:string, direccion_origen:string, destinatario:string, direccion_destino:string,telefono_remitente:string, peso:string){

    let nom_arch:string = "Guia_Local_Post-" + folio;
    
    const pdf = new PdfMakeWrapper();
    //configuraciones del PDF
    pdf.pageSize('A5');
    pdf.pageOrientation('landscape');
    pdf.pageMargins([ 40, 60, 40, 60 ]);
    //Comenzamos la declaración de la imagen para usarla en la guia
    new Img('/assets/img/logo_guia.png').build().then( img => {

    //Creamos el contenido para el PDF
    pdf.add(
      //new QR(tracked).end
      new Table([
        [
          new QR(tracked).end,
          new Stack([
            //Aqui agregamos la imagen del logo
            img,
            new Txt('N° de guía').fontSize(20).bold().end, 
            new Txt(folio).fontSize(15).bold().end
          ]).alignment('center').end,
          new QR(tracked).end 
        ]
      ]).layout('noBorders').widths([80, 290, 80]).end
    );
    pdf.add(
      pdf.ln(2)
    );
    pdf.add(
      //Datos de la guía
      new Table([
        [ 
          new Txt('Remitente: ').bold().end,
          //Datos variables 
          remitente
        ],
        [ 
          new Txt('Dirección Remitente: ').bold().end, 
          direccion_origen
        ],
        [ 
          new Txt('Destinatario: ').bold().end, 
          destinatario
        ],
        [ 
          new Txt('Dirección Destinatario: ').bold().end, 
          direccion_destino
        ],
        [ 
          new Txt('Teléfono: ').bold().end, 
          telefono_remitente
        ],
        [ 
          new Txt('Peso: ').bold().end, 
          peso
        ]
      ]).layout('noBorders').end
    );
    // copia de la guia comienza desde aqui
    pdf.add(
      new Table([
        [
          new QR(tracked).end,
          new Stack([
            img,
            new Txt('N° de guía').fontSize(20).bold().end, 
            new Txt(folio).fontSize(15).bold().end
          ]).alignment('center').end,
          new QR(tracked).end 
        ]
      ]).layout('noBorders').widths([80, 280, 80]).end
    );
    pdf.add(
      pdf.ln(2)
    );
    pdf.add(
      //Datos de la guía
      new Table([
        [ 
          new Txt('Remitente: ').bold().end,
          //Datos variables 
          remitente
        ],
        [ 
          new Txt('Dirección Remitente: ').bold().end, 
          direccion_origen
        ],
        [ 
          new Txt('Destinatario: ').bold().end, 
          destinatario
        ],
        [ 
          new Txt('Dirección Destinatario: ').bold().end, 
          direccion_destino
        ],
        [ 
          new Txt('Teléfono: ').bold().end, 
          telefono_remitente
        ],
        [ 
          new Txt('Peso: ').bold().end, 
          peso
        ]
      ]).layout('noBorders').end
    );

    //Configuramos salida del archivo
    pdf.create().download(nom_arch);
    //pdf.create().open();
    });
  }
}
