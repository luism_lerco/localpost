import { Injectable } from '@angular/core';
import { LoginInterface } from '../../modelos/login.interface';
import { ResponseInterface } from '../../modelos/response.interface';
import { registerInterface} from '../../modelos/register.interface';
import { nuevoEnvioInterface} from '../../modelos/nuevoenvio.interface';
import { UsuarioInterface } from 'src/app/modelos/usuario.interface';
import { rastreoInterface} from 'src/app/modelos/rastreo.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  //url:string = "https://erp-localpost.lerco.agency/web/v1/cliente/";
  url:string = "https://admin.localpost.com.mx/web/v1/cliente/";
  //url2:string = "https://erp-localpost.lerco.agency/web/v1/preenvio/";
  url2:string = "https://admin.localpost.com.mx/web/v1/preenvio/";
  //url:string = "https://api.solodata.es/";

  constructor(private http:HttpClient) {}

    headers: HttpHeaders = new HttpHeaders({
      "Content-Type": "application/json"
    })
  

    loginByEmail(form:LoginInterface):Observable<ResponseInterface>{
      let direccion = this.url + "login";
      //let direccion = this.url + "auth";
      return this.http.post<ResponseInterface>(direccion, form);
    }

    registerUser(form:registerInterface):Observable<ResponseInterface>{
      let direccion = this.url + "register";
      return this.http.post<ResponseInterface>(direccion, form);
    }

    registerEnvio(form:nuevoEnvioInterface):Observable<ResponseInterface>{
      let direccion = this.url2 + "create-preenvio";
      return this.http.post<ResponseInterface>(direccion, form);
    }

    getAllEnvios(id:number):Observable<ResponseInterface>{
      let direccion = this.url2 + "get-envios?id=" + id;
      return this.http.get<ResponseInterface>(direccion);
    }

    getUsuario(id:number):Observable<ResponseInterface>{
      let direccion = this.url + "get-cliente?id=" + id;
      return this.http.get<ResponseInterface>(direccion);
    }

    updateCliente(form:UsuarioInterface):Observable<ResponseInterface>{
      let direccion = this.url + "update-cliente";
      return this.http.post<ResponseInterface>(direccion, form);
    }

    rastreoPaquete(id:number):Observable<ResponseInterface>{
      let direccion = this.url2 + "rastreo?folio=" + id;
      return this.http.get<ResponseInterface>(direccion);
    }
  
}
