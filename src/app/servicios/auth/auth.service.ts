import { Injectable } from '@angular/core';
import { registerInterface } from 'src/app/modelos/register.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url:string = "https://erp-localpost.lerco.agency/web/v1/cliente/";

  constructor(private http:HttpClient) { }
  headers: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json"
  })

  registerUser(form:registerInterface){
    let direccion = this.url + "register";
    return this.http.post(direccion, form);
  }

  setToken() {

  }

  getToken() {
    return "token";
  }
}
