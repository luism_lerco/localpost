import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './components/user/inicio/inicio.component';
import { AvisoDePrivacidadComponent } from './components/aviso-de-privacidad/aviso-de-privacidad.component';
import { FaqComponent } from './components/faq/faq.component';
import { HomeComponent } from './components/home/home.component';
import { Page404Component } from './components/page404/page404.component';
import { RastreoComponent } from './components/rastreo/rastreo.component';
import { TerminosYCondicionesComponent } from './components/terminos-y-condiciones/terminos-y-condiciones.component';
import { LoginComponent } from './components/user/login/login.component';
import { MisEnviosComponent } from './components/user/mis-envios/mis-envios.component';
import { NuevoEnvioComponent } from './components/user/nuevo-envio/nuevo-envio.component';
import { RegisterComponent } from './components/user/register/register.component';
import { MisDatosComponent } from './components/user/mis-datos/mis-datos.component';
import { VigilanteGuard } from './vigilante.guard';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'rastreo', component: RastreoComponent},
  {path: 'faq', component: FaqComponent},
  {path: 'aviso-de-privacidad', component: AvisoDePrivacidadComponent},
  {path: 'terminos-y-condiciones', component: TerminosYCondicionesComponent},
  {path: 'user/login', component: LoginComponent},
  {path: 'user/register', component: RegisterComponent},
  {path: 'user/inicio', component: InicioComponent},
  {path: 'user/mis-envios', component: MisEnviosComponent},
  {path: 'user/nuevo-envio', component: NuevoEnvioComponent},
  {path: 'user/mis-datos', component: MisDatosComponent},
  {path: '**', component: Page404Component}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
