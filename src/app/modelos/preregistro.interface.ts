export interface preregistroInterface {
    token : any;
    tipo_envio : string;
    cliente_emisor_id : any;
    receptor : string;
    direccion_origen : string;
    estado_origen : string;
    ciudad_origen : string;
    cp_origen : any;
    embalaje : string;
    direccion_destino : string;
    estado_destino : string;
    ciudad_destino : string;
    cp_destino : string;
    fecha_recoleccion : any;
    fecha_entrega : any;
    peso : any;
    status : any;
    factura : any;
    comentarios : string;
    subtotal : any;
    impuesto : any;
    total : string;
}