export interface nuevoEnvioInterface {
    token : any ;
    tipo_envio : string;
    cliente_emisor_id : any;
    receptor : string;
    direccion_origen : string;
    estado_origen : string;
    ciudad_origen : string;
    cp_origen : string;
    embalaje : string;
    direccion_destino : string;
    estado_destino : string;
    ciudad_destino : string;
    cp_destino : string;
    telefono_receptor : any;
    fecha_recoleccion : any;
    fecha_entrega : any;
    peso : any;
    factura : any;
    
    comentarios : any;
    subtotal: any;
    impuesto: any;
    pago: any;
    tipo_pago: any;
}