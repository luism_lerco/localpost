export interface UsuarioInterface{
    token : any;
    id : any;
    nombre : string;
    apellidos : string;
    email : string;
    empresa : string;
    giro_empresa : string;
    cargo : string;
    //rfc : string;
    telefono : any;
    password : string;
}