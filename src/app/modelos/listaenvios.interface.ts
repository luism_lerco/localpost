export interface ListaEnviosInterface{
    folio:string;
    tipoEnvio: string;
    tracked: string;
    remitente:string;
    direccion_origen:string;
    destinatario: string;
    direccion_destino: string;
    telefono_remitente: any;
    peso: any;
    monto: any;
    estatus: any;
}