// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //sandbox
  //clientId: 'ASDs9LqAuyH6IpIAKX2yJanzR532eQ8paR22snDTxBnFS1i_j326pYzurbHfUXFZAchOMI5OPMIfYrfk'
  //produccion
  clientId: 'AbgXkbYzUNpogMD7gA0mmxKeZflYj1_Qyiip-56R-IH7hBG0lvTR3kfTm2ZqYYKm5u2Aldl0Q32VUE7a'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
